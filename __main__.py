import argparse
from pathlib import Path
from typing import Sequence
import logging

import hasm

ASK = None
ASK_groups: dict[str, bool] = {}

logger = logging.getLogger(__name__)

class ASKAction(argparse.Action):
	yes: list[str]
	no: list[str]
	single: list[str]
	def __init__(self, option_strings: Sequence[str], dest: str, nargs: int | str | None = None) -> None:
		if nargs is not None:
			raise ValueError("nargs is not supported")
	
		option_strings = list(option_strings)
		
		if list(filter(lambda i: not i.startswith("-"),option_strings)):
			raise ValueError("All option strings must start with a dash")

		self.yes = option_strings.copy()

		self.no = [
			f"--no-{arg.removeprefix('--')}"
			for arg in option_strings
			if arg.startswith("--")
		]

		option_strings.extend(self.no)

		self.single = [arg[1] for arg in option_strings if len(arg) == 2]

		option_strings.extend(
			map(
				lambda single: "-" + 2*single,
				self.single
			)
		)

		if dest == "yes":
			self.no = ["--no"]
			option_strings = "-Y -YY --yes --no".split()

		super().__init__(option_strings, dest, nargs=0)
	def __call__(self, _, __, k: str, ___) -> None:
		global ASK
		if self.dest == "yes":
			v = ASK
		else:
			v = ASK_groups.get(self.dest)

		for _ in (0,):
			if k in map(lambda i: "-"+i,self.single):
				v = not v
				break
			if k in map(lambda i: "-"+i*2,self.single):
				if v is None:
					v = False
				break
			if k in self.yes:
				v = True
			if k in self.no:
				v = False
		ASK_groups[self.dest] = not ASK_groups.get(self.dest,False)
	

def argparse_stuff():
	global parser

	parser = argparse.ArgumentParser()
	parser.add_argument("-Y","--yes",action=ASKAction)
	parser.add_argument("-O","--override",action=ASKAction)

	subparsers = parser.add_subparsers(required=True)

	parser_hasm = subparsers.add_parser("hasm")
	parser_hasm.add_argument("input",type=Path)
	parser_hasm.add_argument("-o","--output",type=Path,required=False,default=Path("./out"))
	parser_hasm.add_argument("-s","--simple",action="store_true")
	parser_hasm.add_argument("-r","--reverse",action="store_true")
	parser_hasm.set_defaults(func=main_hasm)


def ask(msg: str, group: str = "") -> bool:
	if ASK is not None:
		return ASK
	if group in ASK_groups:
		return ASK_groups[group]
	while True:
		i = input(msg + " ([Y]es/[N]o/[A]lways/ne[V]er):")
		if i.lower() in "y yes".split():
			return True
		if i.lower() in "n no".split():
			return False
		if i.lower() in "a always all".split():
			ASK_groups[group] = True
			return True
		if i.lower() in "v never".split():
			ASK_groups[group] = False
			return False


def main_hasm(ns: argparse.Namespace):
	# extract arguments to add static typing
	inp: Path = ns.input
	out: Path = ns.output
	simple: bool = ns.simple
	reverse: bool = ns.reverse

	if reverse and out == Path("./out"):
		out = Path("./out.hasm")
	
	# TODO(#4): better decompilation
	if reverse:
		assert simple, "Only simple decompilation is supported at the moment (did you mean to add the `-s`/`--simple` flag?)"
	
	assert inp.exists(), f"{inp} does not exist or is not accessable"
	if out.exists():
		a = ask(f"{out} exists. Overide?")
		assert a, f"{out} already exists."
	
	if not reverse:
		with inp.open("r") as finp, out.open("wb") as fout:
			code = finp.read()
			tokens = hasm.lex(code)
			fout.write(hasm.parse_n_dump(tokens,code))
		return
	elif simple:
		with inp.open("rb") as finp, out.open("w") as fout:
			fout.write(hasm.simple_decompiler(finp.read()))
		return


def main():
	argparse_stuff()

	ns = parser.parse_args()
	logging.basicConfig(format="%(levelname)s: %(message)s")
	try:
		ns.func(ns)
	except AssertionError as ae:
		logger.fatal(str(ae))

if __name__ == "__main__":
	main()
