import ha_types as types

def dumpv(value: str | types.short | types.long):
	if isinstance(value,str):
		return dump_str(value)
	return value.dump()

def dump_str(value: str):
	return (len(value)*2).to_bytes(1,"little") + b"\0" + b"\0".join(bytes(i, "ascii") for i in value) +b"\0"

def dump(data: dict[str, str | types.short | types.long]):
	"""
	allows dumping dicts
	if a value is static use hasm instead
	
	note: items must me str or from ha_types
	"""

	out = b""

	for k,v in data.items():
		out += dump_str(k) + b"\0" + dumpv(v) + b"\0"
	
	return out

