import string
from typing import TypeAlias, Literal, NamedTuple
import sys
import dumper

CODE = ""

Pos = NamedTuple("Pos",[("line",int),("coll",int)])


def code_iter(code: str):
	c = 0
	l = 0
	for i in code:
		if i == "\n":
			c = 0
			l += 1
		else:
			c += 1
		yield i, Pos(l,c)

	while True:
		yield None, Pos(l,c)

class Error(Exception):
	msg: str
	pos: Pos
	code: str

	def __init__(self,msg: str, pos: Pos, code: str):
		self.msg = msg
		self.pos = pos
		self.code = code
	
	def __str__(self):
		return f"{self.msg} @{self.pos.line}:{self.pos.coll}"

_exception_hook = sys.excepthook
def exception_hook(exc_type,exc_value,exc_traceback):
	if exc_type == Error:
		print("Error:", exc_value.msg, f"@{exc_value.pos.line}:{exc_value.pos.coll}")
		lines = exc_value.code.splitlines()
		if exc_value.pos.line >= len(lines):
			return
		
		print(f"{exc_value.pos.line: >3} |", lines[exc_value.pos.line])
		pre = "".join(map(lambda c: " " if c != "\t" else "\t",lines[exc_value.pos.line][exc_value.pos.coll:]))
		print("   "+pre,"^ Here",sep="")
		return
	return _exception_hook(exc_type,exc_value,exc_traceback)

sys.excepthook = exception_hook

TType: TypeAlias = Literal["string", "byte", "short", "long", "null", "key"]

TToken = NamedTuple("TToken",[("type", TType),("value", str), ("pos", Pos)])

TNames: dict[str,TType] = {
	"l":'long',
	"s":'short',
	"*":'byte'
}

escapes = {
	"0": "\0",
	"n": "\n",
	"t": "\t",
}

def lex(code: str) -> list[TToken]:
	citer = code_iter(code)

	tokens: list[TToken] = []
	current = ""
	pre = None
	rot = False
	c = ""
	p = Pos(0,0)

	while True:
		if not rot:
			c, p = next(citer)
		rot = False
		
		if c is None:
			break
		current = ""

		if c == "#":
			while c is not None and c != "\n":
				c,p = next(citer)
			continue

		if c == "\"":
			if current != "":
				raise Error("Unexpected \"",p ,code)
			while True:
				c, _ = next(citer)
				if c is None or c == "\n":
					raise Error("Unclosed \"",p, code)

				if c == "\"":
					break

				if c == "\\":
					c, p_ = next(citer)
					if c is None:
						raise Error("Invalid escape", p_, code)
					if c in "\n\t ":
						continue
					c = escapes.get(c,c)

				current += c
			tokens.append(TToken("string",current,p))
			continue
		
		if c.lower() in TNames:
			if current != "":
				raise Error(f"Unexpected `{c}`", p, code)
			while True:
				c2, p2 = next(citer)
				if c2 is None or c2 in " \t\n.!" or c2.lower() in TNames:
					break
				current += c2
			tokens.append(TToken(TNames[c.lower()],current,p))
			current = ""
			c,p = c2,p2
			if c is None:
				break
		
		if c == "&":
			while True:
				c2, p2 = next(citer)
				if c2 is None or c2 not in (string.ascii_letters + string.digits + "-_&"):
					break
				if c2 == "&" and "&" in current:
					raise Error("Only level 1 and 2 keys are supported.",p2,code)
				current += c2
			if c2 == ":":
				# type report
				current += ":"
				c2, p2 = next(citer)
				if c2 is None:
					raise Error("expected a type declaration, got EOF",p2,code)
				while c2 in TNames or c2 in string.digits:
					current += c2

					c2, p2 = next(citer)
					if c2 is None: break
				if c2 is None: break

				if current.endswith(":"):
					if c2 in ".!":
						raise Error("Cannot use null as a key type",p2,code)
					raise Error(f"Expected key type declaration", p2, code)
				if current.endswith("*"):
					raise Error("The smallest type for a key is (S)hort",p2,code)
			tokens.append(TToken("key",current,p))
			c,p = c2,p2
			if c is None:
				break

		if c in ".!":
			tokens.append(TToken("null","",p))
			continue

		if c in string.hexdigits:
			c2, p2 = next(citer)
			if c2 is None:
				raise Error("Expected hexdigit (0-9 a-f A-F), got EOF",p2,code)
			pc2 = None
			if c2 in " \n\t.!" or c2 in TNames:
				pc2 = c2
				c2 = c
				c = '0'

			if c2 not in string.hexdigits:
				raise Error("Expected hexdigit (0-9 a-f A-F), got `{c2}`",p2,code)
			tokens.append(TToken("byte",f"0x{c}{c2}",p))
			if pc2 is None:
				continue
			c, p = pc2, p2
	
		if c == "'":
			c2,p2 = next(citer)
			if c2 is None:
				raise Error("Expected character, got EOF",p2,code)
			if c2 == "\\":
				c2,p2 = next(citer)
				if c2 is None or c2 in "\n\t ":
					raise Error("Invalid escape (Did you want to use a string (`\"<text>\"`)?)",p2,code)
				c2 = escapes.get(c2,c2)
			tokens.append(TToken("byte",f"{ord(c2)}",p))
			continue

		if c in "\n\t ":
			continue
		
		if pre == p:
			raise Error(f"Unexpected `{c}`",p,code)
		pre = p
		rot = True

	if current != "":
		raise Error(f"Unexpected EOF (`{current}` left hanging)",p,code)
	
	return tokens

def dump(token: TToken,code) -> bytes:
	if token.type == "string":
		return dumper.dump_str(token.value)
	if token.type in ("byte","short","long"):
		return dump_int(token,code)
	if token.type == "null":
		return b"\0"
	raise ValueError("cannot handle keys")

irange = {
	"byte": range(-127,255),
	"short": range(-0x8000, 0xffff),
	"long": range(-0x60000000, 0xffffffff)
}
lengths = {
	"byte": 1,
	"short": 2,
	"long": 4
}

def dump_int(token: TToken,code) -> bytes:
	try:
		value = int(token.value,0)
	except ValueError:
		raise Error("Invalid literal",token.pos,code)
	# check if in range
	assert value in irange[token.type]
	return value.to_bytes(lengths[token.type],"little")

metakey_parts = ["string","short","long"]

def parse_n_dump(tokens: list[TToken],code: str) -> bytes:
	out = b""
	metakeys: dict[str,list[int]] = {}

	tokens = tokens.copy()

	while tokens:
		token = tokens.pop(0)
		if token.type != "key":
			out += dump(token,code)
			continue
		if ":" in token.value:
			key,ktype = token.value.split(":")
		else:
			key = token.value
			ktype = None
		# figure out the value
		if not ktype:
			try:
				value = tokens.pop(0)
			except IndexError:
				raise Error("Expected key value, got EOF",token.pos,code)  # from None
			ktype = value.type
			if value.type == "key":
				raise Error("Expected key value",value.pos,code)
			if value.type in ("byte","null"):
				raise Error("Keys cannot be set to single byte values (maybe try defining the keys type?)",value.pos,code)
			bvalue = dump(value,code)
		else:
			if ktype.lower() in ("s","l"):
				vlen = lengths[TNames[ktype]]
				ktype = TNames[ktype]
			else:
				try:
					vlen = int(ktype)
				except ValueError:
					raise Error(f"Unable to parse key type (`{ktype}`)",token.pos,code) from None
				ktype = "string"
			bvalue = b""
			vtoken = None
			pp = token.pos
			while len(bvalue) < vlen:
				try:
					vtoken = tokens.pop(0)
				except IndexError:
					raise Error("Expected continuation of key value, got EOF",(vtoken or token).pos,code) from None
				if vtoken.type == "key":
					raise Error(f"Expected continuation of key value (reached {len(bvalue)}B/{vlen}B)",pp,code)
				if vtoken.type == "string":
					bvalue += dumper.dump_str(vtoken,True)
					continue
				bvalue += dump(vtoken,code)
				pp = vtoken.pos
			if len(bvalue) != vlen:
				raise Error("Could not allgin end of value to desired lenght",(vtoken or token).pos,code)
					
		if "&" in key:
			metakey, key = key.split("&")
			if metakey not in metakeys:
				metakeys[metakey] = [0,0,0]
			index = metakeys[metakey][metakey_parts.index(ktype)]
			metakeys[metakey][metakey_parts.index(ktype)] += 1
			out += dumper.dump_str(f"{metakey}_{ktype}{index}_key")
			out += dumper.dump_str(key)
		
			key = f"{metakey}_{ktype}{index}_value"
		
		out += dumper.dump_str(key)
		out += bvalue
	
	for metakey,parts in metakeys.items():
		for pname, part in zip(metakey_parts,parts):
			out += dumper.dump_str(f"{metakey}_{pname}_count")
			out += part.to_bytes(1,"little")

	return out

def simple_decompiler(data: bytes) -> str:
	out = ""

	for i in data:
		if i == 0:
			out += "."
			continue
		if chr(i) in string.printable[:-5]:
			out += "'" + chr(i)
			continue
		if chr(i) in "\t\n":
			out += "'" + repr(chr(i))[1:2]
			continue
		out += f"{i:x}"
	return out
