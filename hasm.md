# HASM

## Raw data

Hexadecimal values can be entered in plain text:

```
B 11 2233
```

A null byte can be also specified with either a dot or exclamation mark (`.` or `!`)

By using a single quote, a character can be specified. (Note: HA strings have characters every second byte for some reason)

```
`A `B `C
`\n `\t `\\
```

To enter a value in a base other than Hexadecimal, prefix the value with a star (`*`):

```
*11 *0b10101
*-20
```

The value will be padded to 8 bits.
For larger sizes `s` (short) or `l` (long) can be used:

```
s11
l11223
S22
L1245
```

Strings can also be used

```
"Hello!"
"This string\
is on multible lines!"
"Line one\nLine two."
```

## Keys

A key is prefixed with a `&` character. It has to be either followed by a string, short or long value:

```
$KeyA "value"
$KeyB s11
$KeyC L21
```

Or have a type declaration with a collon:

```
$KeyD:s 11 21
$KeyE:L s10 s13
$KeyF:12 "Hello!"
```

### Second layer keys

Second layer keys are also supported:

```
$obj$key s12
```

